from typing import Optional
from datetime import datetime

class Note(object):
    CONST_DEFAULT_PER = 20

    subject: str
    date: datetime
    title: Optional[str]
    note: Optional[float]
    note_per: float
    note_average: float
    note_average_per: float
    best_note: float
    best_note_per: float
    not_good_note: float
    not_good_note_per: float
    coefficient: int

    def __init__(self, subject: str, date: str, title: Optional[str], note: str, note_average: str, best_note: str, not_good_note: str, coefficient: int):
        self.subject = subject
        self.date = datetime.strptime(date, '%d/%m')

        if "2018-08-28" < self.date.strftime("2018-%m-%d"):
            self.date = self.date.replace(year=2018)
        else:
            self.date = self.date.replace(year=2019)

        self.title = title

        self.note = self.__string2note(note)
        self.note_per = self.__string_note2per(note)

        self.note_average = self.__string2note(note_average)
        self.note_average_per = self.__string_note2per(note_average)

        self.best_note = self.__string2note(best_note)
        self.best_note_per = self.__string_note2per(best_note)

        self.not_good_note = self.__string2note(not_good_note)
        self.not_good_note_per = self.__string_note2per(not_good_note)

        self.coefficient = coefficient

    def __string_note2per(self, note: str) -> Optional[float]:

        if note == 'N.Rdu':
            return

        if note.find('/') > 0:
            split = note.split('/')
            return float(split[1])

        return self.CONST_DEFAULT_PER

    def __string2note(self, note: str) -> Optional[float]:

        if note == 'N.Rdu':
            return

        note = note.replace(',', '.')

        if note.find('/') > 0:
            split = note.split('/')
            return float(split[0]) / float(split[1])

        return float(note) / self.CONST_DEFAULT_PER

    def show(self):
        print("Subject      : " + self.subject)
        print("Date         : " + self.date.strftime("%d %B, 2018"))
        print("Title        : " + self.title)

        if self.note is None:
            print("Note         : Not rated")
        else:
            print("Note         : " + str(self.note * self.note_per) + " / " + str(self.note_per))

        print("Note Average : " + str(self.note_average * self.note_average_per) + " / " + str(self.note_average_per))
        print("Note +       : " + str(self.best_note * self.best_note_per) + " / " + str(self.not_good_note_per))
        print("Note -       : " + str(self.not_good_note * self.not_good_note_per) + " / " + str(self.best_note_per))
        print("Coefficient  : " + str(self.coefficient))
