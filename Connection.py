from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import Navigate
import os

def connection(driver):
    driver.get('https://metice.ac-reunion.fr/')

    id_box = driver.find_element_by_name('username')

    id_box.send_keys(os.environ['USERNAME'])

    password_box = driver.find_element_by_name('password')

    password_box.send_keys(os.environ['PASSWORD'])

    try:
        element = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((
                By.XPATH, '//*[@id="row_etab"]/div[1]/button')))

        print(element)

        element.click()

        driver.find_element_by_xpath('//*[@id="row_etab"]/div[1]/div/ul/li['+os.environ['SCHOOL_LIST_ID']+']/a/span').click()
    finally:

        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="valider"]'))).click()

        WebDriverWait(driver, 30).until(
            lambda driver: driver.current_url != "https://portail.louispayen.ac-reunion.fr/envole/portal/mypage.php")

        Navigate.home_page(driver)
