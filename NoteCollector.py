from typing import List

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import Navigate


from Note import Note



def get_all(driver):
    Navigate.home_page(driver)

    driver.find_element_by_xpath('// *[ @ id = "GInterface.Instances[0].Instances[0]_Wrapper"] / li[3]').click()

    # /html/body/div[4]/table/tbody/tr[2]/td/table/tbody/tr/td[1]/div/div[2]/div[2]/div[1]/div[1]/div[2]/div/div/table/tbody

    table = WebDriverWait(driver, 10).until((
        EC.element_to_be_clickable((
            By.XPATH,
            '/html/body/div[4]/table/tbody/tr[2]/td/table/tbody/tr/td[1]/div/div[2]/div[2]/div[1]/div[1]/div[2]/div/div/table/tbody'
        ))
    ))

    rows = table.find_elements(By.TAG_NAME, "tr")

    notes: List[Note] = []

    for row in rows:
        # Get the columns (all the column 2)
        cols = row.find_elements(By.TAG_NAME, "td")

        if len(cols) >= 2:

            col = cols[1]

            if col.text == "":
                continue

            driver.execute_script("arguments[0].scrollIntoView(true);", col)
            col.click()

            data = get_screen_information(driver)

            if type(data) == Note:
                notes.append(data)

    return notes


def get_screen_information(driver) -> Note:
    subject = driver.find_element_by_xpath(
        '/html/body/div[4]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/div/div[1]/table/tbody/tr/td').text

    if '- Note' in subject:

        table = driver.find_element_by_xpath('/html/body/div[4]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/div/div[2]/div/table/tbody')

        title = ""

        date = subject.split(' - Note du ')[1]
        subject = subject.split(' - Note du ')[0]

        if len(table.find_elements(By.TAG_NAME, 'tr')) == 6:
            title = driver.find_element_by_xpath('/html/body/div[4]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/div/div[2]/div/table/tbody/tr[1]').text

            note = driver.find_element_by_xpath('/html/body/div[4]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/div/div['
                                                '2]/div/table/tbody/tr[2]/td[1]/table/tbody/tr[1]/td[2]').text

            best_note = driver.find_element_by_xpath('/html/body/div[4]/table/tbody/tr[2]/td/table/tbody/tr/td['
                                                     '2]/div/div[2]/div/table/tbody/tr[2]/td[1]/table/tbody/tr[3]/td[2]').text

            not_good_note = driver.find_element_by_xpath('/html/body/div[4]/table/tbody/tr[2]/td/table/tbody/tr/td['
                                                         '2]/div/div[2]/div/table/tbody/tr[2]/td[1]/table/tbody/tr[4]/td[2]').text

            note_average = driver.find_element_by_xpath('/html/body/div[4]/table/tbody/tr[2]/td/table/tbody/tr/td['
                                                        '2]/div/div[2]/div/table/tbody/tr/td[1]/table/tbody/tr[2]/td[2]').text

            coefficient = driver.find_element_by_xpath('/html/body/div[4]/table/tbody/tr[2]/td/table/tbody/tr/td['
                                                       '2]/div/div[2]/div/table/tbody/tr[2]/td[2]/div').text.split(': ')[1]

        else:
            note = driver.find_element_by_xpath('/html/body/div[4]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/div/div['
                                                '2]/div/table/tbody/tr/td[1]/table/tbody/tr[1]/td[2]').text

            best_note = driver.find_element_by_xpath('/html/body/div[4]/table/tbody/tr[2]/td/table/tbody/tr/td['
                                                     '2]/div/div[2]/div/table/tbody/tr/td[1]/table/tbody/tr[3]/td[2]').text

            not_good_note = driver.find_element_by_xpath('/html/body/div[4]/table/tbody/tr[2]/td/table/tbody/tr/td['
                                                         '2]/div/div[2]/div/table/tbody/tr/td[1]/table/tbody/tr[4]/td[2]').text

            note_average = driver.find_element_by_xpath('/html/body/div[4]/table/tbody/tr[2]/td/table/tbody/tr/td['
                                                        '2]/div/div[2]/div/table/tbody/tr/td[1]/table/tbody/tr[2]/td[2]').text

            coefficient = driver.find_element_by_xpath('/html/body/div[4]/table/tbody/tr[2]/td/table/tbody/tr/td['
                                                       '2]/div/div[2]/div/table/tbody/tr/td[2]/div').text.split(': ')[1]

        note = Note(subject, date, title, note, note_average, best_note, not_good_note, coefficient)

        return note
