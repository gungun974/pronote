from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from time import sleep


def home_page(driver):
    driver.get('https://9741050y.index-education.net/pronote/')

    WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((
            By.XPATH, '// *[ @ id = "GInterface.Instances[0].Instances[0]_Wrapper"] / li[3]')))

    sleep(3)
