from time import sleep

from selenium import webdriver

import Connection
import NoteCollector
import locale

from datetime import datetime
from elasticsearch import Elasticsearch
import json

from Note import Note

# driver = webdriver.Firefox('C:\\Program Files\\Mozilla Firefox')

if __name__ == "__main__":

    es = Elasticsearch()

    driver = webdriver.Chrome()

    locale.setlocale(locale.LC_TIME, 'fr')


    class DateTimeEncoder(json.JSONEncoder):
        def default(self, o):
            if isinstance(o, datetime):
                return o.timestamp()

            return json.JSONEncoder.default(self, o)


    # Open the website

    Connection.connection(driver)

    notes = NoteCollector.get_all(driver)

    note: Note
    for note in notes:
        data = note.__dict__

        try:

            res = es.search(index="pronote-index", body={
                "query": {
                    "bool": {
                        "should": [
                            {
                                "term": {
                                    "subject": {
                                        "value": note.subject
                                    }
                                }
                            },
                            {
                                "term": {
                                    "date": {
                                        "value": note.date.timestamp()
                                    }
                                }
                            },
                            {
                                "term": {
                                    "title": {
                                        "value": note.title
                                    }
                                }
                            }
                        ]
                    }
                }
            })

        except:
            res = {"hits": {"total": 0}}

        if res["hits"]["total"] < 1:
            data['timestamps'] = datetime.now()

            res = es.index(index="pronote-index", doc_type='note', body=json.dumps(data, cls=DateTimeEncoder))
            print(res['result'])

    sleep(0.5)

    driver.quit()
